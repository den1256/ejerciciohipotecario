
package com.Denis.EjercicioHipotecario.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "values",
    "attribute_group_id",
    "attribute_group_name",
    "id",
    "value_id",
    "value_name",
    "value_struct",
    "source",
    "name"
})
public class Attribute {

    @JsonProperty("values")
    private List<Value> values = null;
    @JsonProperty("attribute_group_id")
    private String attributeGroupId;
    @JsonProperty("attribute_group_name")
    private String attributeGroupName;
    @JsonProperty("id")
    private String id;
    @JsonProperty("value_id")
    private String valueId;
    @JsonProperty("value_name")
    private String valueName;
    @JsonProperty("value_struct")
    private Object valueStruct;
    @JsonProperty("source")
    private Integer source;
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("values")
    public List<Value> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<Value> values) {
        this.values = values;
    }

    @JsonProperty("attribute_group_id")
    public String getAttributeGroupId() {
        return attributeGroupId;
    }

    @JsonProperty("attribute_group_id")
    public void setAttributeGroupId(String attributeGroupId) {
        this.attributeGroupId = attributeGroupId;
    }

    @JsonProperty("attribute_group_name")
    public String getAttributeGroupName() {
        return attributeGroupName;
    }

    @JsonProperty("attribute_group_name")
    public void setAttributeGroupName(String attributeGroupName) {
        this.attributeGroupName = attributeGroupName;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("value_id")
    public String getValueId() {
        return valueId;
    }

    @JsonProperty("value_id")
    public void setValueId(String valueId) {
        this.valueId = valueId;
    }

    @JsonProperty("value_name")
    public String getValueName() {
        return valueName;
    }

    @JsonProperty("value_name")
    public void setValueName(String valueName) {
        this.valueName = valueName;
    }

    @JsonProperty("value_struct")
    public Object getValueStruct() {
        return valueStruct;
    }

    @JsonProperty("value_struct")
    public void setValueStruct(Object valueStruct) {
        this.valueStruct = valueStruct;
    }

    @JsonProperty("source")
    public Integer getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(Integer source) {
        this.source = source;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
