
package com.Denis.EjercicioHipotecario.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "site_id",
    "title",
    "seller",
    "price",
    "currency_id",
    "available_quantity",
    "sold_quantity",
    "buying_mode",
    "listing_type_id",
    "stop_time",
    "condition",
    "permalink",
    "thumbnail",
    "accepts_mercadopago",
    "installments",
    "address",
    "shipping",
    "seller_address",
    "attributes",
    "differential_pricing",
    "original_price",
    "category_id",
    "official_store_id",
    "catalog_product_id",
    "tags",
    "catalog_listing"
})
public class Busqueda {

    @JsonProperty("id")
    private String id;
    @JsonProperty("site_id")
    private String siteId;
    @JsonProperty("title")
    private String title;
    @JsonProperty("seller")
    private Seller seller;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("currency_id")
    private String currencyId;
    @JsonProperty("available_quantity")
    private Integer availableQuantity;
    @JsonProperty("sold_quantity")
    private Integer soldQuantity;
    @JsonProperty("buying_mode")
    private String buyingMode;
    @JsonProperty("listing_type_id")
    private String listingTypeId;
    @JsonProperty("stop_time")
    private String stopTime;
    @JsonProperty("condition")
    private String condition;
    @JsonProperty("permalink")
    private String permalink;
    @JsonProperty("thumbnail")
    private String thumbnail;
    @JsonProperty("accepts_mercadopago")
    private Boolean acceptsMercadopago;
    @JsonProperty("installments")
    private Installments installments;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("shipping")
    private Shipping shipping;
    @JsonProperty("seller_address")
    private SellerAddress sellerAddress;
    @JsonProperty("attributes")
    private List<Attribute> attributes = null;
    @JsonProperty("differential_pricing")
    private DifferentialPricing differentialPricing;
    @JsonProperty("original_price")
    private Object originalPrice;
    @JsonProperty("category_id")
    private String categoryId;
    @JsonProperty("official_store_id")
    private Object officialStoreId;
    @JsonProperty("catalog_product_id")
    private String catalogProductId;
    @JsonProperty("tags")
    private List<String> tags = null;
    @JsonProperty("catalog_listing")
    private Boolean catalogListing;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("site_id")
    public String getSiteId() {
        return siteId;
    }

    @JsonProperty("site_id")
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("seller")
    public Seller getSeller() {
        return seller;
    }

    @JsonProperty("seller")
    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("currency_id")
    public String getCurrencyId() {
        return currencyId;
    }

    @JsonProperty("currency_id")
    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    @JsonProperty("available_quantity")
    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    @JsonProperty("available_quantity")
    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    @JsonProperty("sold_quantity")
    public Integer getSoldQuantity() {
        return soldQuantity;
    }

    @JsonProperty("sold_quantity")
    public void setSoldQuantity(Integer soldQuantity) {
        this.soldQuantity = soldQuantity;
    }

    @JsonProperty("buying_mode")
    public String getBuyingMode() {
        return buyingMode;
    }

    @JsonProperty("buying_mode")
    public void setBuyingMode(String buyingMode) {
        this.buyingMode = buyingMode;
    }

    @JsonProperty("listing_type_id")
    public String getListingTypeId() {
        return listingTypeId;
    }

    @JsonProperty("listing_type_id")
    public void setListingTypeId(String listingTypeId) {
        this.listingTypeId = listingTypeId;
    }

    @JsonProperty("stop_time")
    public String getStopTime() {
        return stopTime;
    }

    @JsonProperty("stop_time")
    public void setStopTime(String stopTime) {
        this.stopTime = stopTime;
    }

    @JsonProperty("condition")
    public String getCondition() {
        return condition;
    }

    @JsonProperty("condition")
    public void setCondition(String condition) {
        this.condition = condition;
    }

    @JsonProperty("permalink")
    public String getPermalink() {
        return permalink;
    }

    @JsonProperty("permalink")
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    @JsonProperty("thumbnail")
    public String getThumbnail() {
        return thumbnail;
    }

    @JsonProperty("thumbnail")
    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @JsonProperty("accepts_mercadopago")
    public Boolean getAcceptsMercadopago() {
        return acceptsMercadopago;
    }

    @JsonProperty("accepts_mercadopago")
    public void setAcceptsMercadopago(Boolean acceptsMercadopago) {
        this.acceptsMercadopago = acceptsMercadopago;
    }

    @JsonProperty("installments")
    public Installments getInstallments() {
        return installments;
    }

    @JsonProperty("installments")
    public void setInstallments(Installments installments) {
        this.installments = installments;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("shipping")
    public Shipping getShipping() {
        return shipping;
    }

    @JsonProperty("shipping")
    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    @JsonProperty("seller_address")
    public SellerAddress getSellerAddress() {
        return sellerAddress;
    }

    @JsonProperty("seller_address")
    public void setSellerAddress(SellerAddress sellerAddress) {
        this.sellerAddress = sellerAddress;
    }

    @JsonProperty("attributes")
    public List<Attribute> getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("differential_pricing")
    public DifferentialPricing getDifferentialPricing() {
        return differentialPricing;
    }

    @JsonProperty("differential_pricing")
    public void setDifferentialPricing(DifferentialPricing differentialPricing) {
        this.differentialPricing = differentialPricing;
    }

    @JsonProperty("original_price")
    public Object getOriginalPrice() {
        return originalPrice;
    }

    @JsonProperty("original_price")
    public void setOriginalPrice(Object originalPrice) {
        this.originalPrice = originalPrice;
    }

    @JsonProperty("category_id")
    public String getCategoryId() {
        return categoryId;
    }

    @JsonProperty("category_id")
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty("official_store_id")
    public Object getOfficialStoreId() {
        return officialStoreId;
    }

    @JsonProperty("official_store_id")
    public void setOfficialStoreId(Object officialStoreId) {
        this.officialStoreId = officialStoreId;
    }

    @JsonProperty("catalog_product_id")
    public String getCatalogProductId() {
        return catalogProductId;
    }

    @JsonProperty("catalog_product_id")
    public void setCatalogProductId(String catalogProductId) {
        this.catalogProductId = catalogProductId;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @JsonProperty("catalog_listing")
    public Boolean getCatalogListing() {
        return catalogListing;
    }

    @JsonProperty("catalog_listing")
    public void setCatalogListing(Boolean catalogListing) {
        this.catalogListing = catalogListing;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
