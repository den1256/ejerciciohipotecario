package com.Denis.EjercicioHipotecario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioHipotecarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(EjercicioHipotecarioApplication.class, args);
	}

}
